package com.dipankar.controller;

import com.dipankar.service.DummyExternalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RetryController {

    @Autowired
    private DummyExternalService service;

    @GetMapping(value = "/retry")
    public String callExternalService(){
            return service.dummyService();

    }

}
