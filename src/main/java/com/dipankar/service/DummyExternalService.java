package com.dipankar.service;

import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.Date;

@Service
public class DummyExternalService {

    @Retryable( retryFor = {Exception.class},
                maxAttempts = 5,
                backoff = @Backoff(delay = 1000,multiplier = 2))
    public String dummyService() {

        final String uri = "http://localhost:8081/hello-world";  // this

        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(5000); // 5 seconds
        factory.setReadTimeout(5000);   // 5 seconds
        RestTemplate restTemplate = new RestTemplate(factory);

        System.out.println("Dummy External Service called at : " + new Date().toString() );
        String result = restTemplate.getForObject(uri, String.class);
        System.out.println("Sending response back at : " + new Date().toString() + " . And response : " + result);

        return "Success";
    }

    @Recover
    public String recover(Exception e) {
        return "Default Data";
    }
}
